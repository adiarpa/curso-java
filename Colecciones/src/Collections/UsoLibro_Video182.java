package Collections;

public class UsoLibro_Video182 {

    public static void main(String[] args) {

        Libro_Video182 libro1 = new Libro_Video182("P en Java","Juan", 25);
        Libro_Video182 libro2 = new Libro_Video182("P en Java II", "Juan", 25);

        //libro1=libro2;

        if (libro1.equals(libro2)){
            System.out.println("Es el mismo libro.");

            System.out.println(libro1.hashCode());
            System.out.println(libro2.hashCode());

        }else{
            System.out.println("No es el mismo libro.");

            System.out.println(libro1.hashCode());
            System.out.println(libro2.hashCode());
        }

    }

}
