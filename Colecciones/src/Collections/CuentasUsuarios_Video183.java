package Collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class CuentasUsuarios_Video183 {

    public static void main(String[] args) {

        Cliente_Video180 cl1 = new Cliente_Video180("Antonio Banderas", "00001", 200000);
        Cliente_Video180 cl2 = new Cliente_Video180("Rafael Nadal", "00002", 250000);
        Cliente_Video180 cl3 = new Cliente_Video180("Penelope Cruz", "00003", 300000);
        Cliente_Video180 cl4 = new Cliente_Video180("Julio Iglesias", "00004", 500000);
        Cliente_Video180 cl5 = new Cliente_Video180("Antonio Banderas", "00001", 200000);

        Set <Cliente_Video180> clientesBancos = new HashSet<Cliente_Video180>();

        clientesBancos.add(cl1);
        clientesBancos.add(cl2);
        clientesBancos.add(cl3);
        clientesBancos.add(cl4);
        clientesBancos.add(cl5);

       /* for (Cliente_Video180 cliente: clientesBancos ) {
            //System.out.println(cliente.getNombre()+" "+cliente.getN_cuenta()+" "+cliente.getSaldo());

            if (cliente.getNombre().equals("Julio Iglesias")){

                clientesBancos.remove(cliente);
            }

        }  */

       Iterator<Cliente_Video180> it = clientesBancos.iterator();

       while (it.hasNext()){
           String nombre_cliente = it.next().getNombre();
           if (nombre_cliente.equals("Julio Iglesias")){
               it.remove();
           }
       }

        for (Cliente_Video180 cliente: clientesBancos ) {
            System.out.println(cliente.getNombre()+" "+cliente.getN_cuenta()+" "+cliente.getSaldo());
        }

        /*Iterator<Cliente_Video180> it = clientesBancos.iterator();

        while (it.hasNext()){
            String nombre_cliente = it.next().getNombre();
            System.out.println(nombre_cliente);
        } */

    }

}
