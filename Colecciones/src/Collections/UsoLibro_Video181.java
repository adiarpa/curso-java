package Collections;

public class UsoLibro_Video181 {

    public static void main(String[] args) {

        Libro_Video181 libro1 = new Libro_Video181("P en Java","Juan", 25);
        Libro_Video181 libro2 = new Libro_Video181("P en Java", "Juan", 25);

        if (libro1.equals(libro2)){
            System.out.println("Es el mismo libro.");
        }else{
            System.out.println("No es el mismo libro.");
        }

    }

}
