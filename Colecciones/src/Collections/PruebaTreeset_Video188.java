package Collections;

import java.util.Comparator;
import java.util.TreeSet;

public class PruebaTreeset_Video188 {
    public static void main(String[] args) {
    /*    TreeSet<String> ordenarPersonas = new TreeSet<String>();

        ordenarPersonas.add("Sandra");
        ordenarPersonas.add("Amanda");
        ordenarPersonas.add("Dinana");

        for (String s: ordenarPersonas) {
            System.out.println(s);
        }*/

      Articulooo primero = new Articulooo(1, "Primer artículo");
      Articulooo segundo = new Articulooo(200, "Segundo artículo");
      Articulooo tercer  = new Articulooo(3, "Este es el tercer artículo");

      TreeSet<Articulooo> ordenaArticulo = new TreeSet<Articulooo>();

      ordenaArticulo.add(tercer);
      ordenaArticulo.add(primero);
      ordenaArticulo.add(segundo);

        for (Articulooo art: ordenaArticulo ) {
            System.out.println(art.getDescripcion());

        }

        //Articulooo comparadorArticulos = new Articulooo();
        //TreeSet<Articulooo> ordenaArticulos2 = new TreeSet<Articulooo>();

       // ComparadorArticulos compara_art = new ComparadorArticulos();
        
        TreeSet<Articulooo> ordenaArticulos2 = new TreeSet<Articulooo>(new Comparator<Articulooo>() {
            @Override
            public int compare(Articulooo o1, Articulooo o2) {
                String desc1 = o1.getDescripcion();
                String desc2 = o2.getDescripcion();
                return desc1.compareTo(desc2);
            }
        });

        ordenaArticulos2.add(primero);
        ordenaArticulos2.add(segundo);
        ordenaArticulos2.add(tercer);

        for (Articulooo art: ordenaArticulos2) {
            System.out.println(art.getDescripcion());
        }

    }
}

class Articulooo implements Comparable<Articulooo> {

    private int numero_articulo;
    private String descripcion;

    public Articulooo(int num, String desc) {
        numero_articulo = num;
        descripcion = desc;

    }

    public String getDescripcion(){
        return descripcion;
    }

    @Override
    public int compareTo(Articulooo o) {
        return numero_articulo - o.numero_articulo;
    }
}

class ComparadorArticulos implements Comparator<Articulooo>{

    @Override
    public int compare(Articulooo o1, Articulooo o2) {
        String desc1 = o1.getDescripcion();
        String desc2 = o2.getDescripcion();
        return desc1.compareTo(desc2);
    }
}
