package Collections;

import java.util.TreeSet;

public class PruebaTreeset_Video186 {
    public static void main(String[] args) {
    /*    TreeSet<String> ordenarPersonas = new TreeSet<String>();

        ordenarPersonas.add("Sandra");
        ordenarPersonas.add("Amanda");
        ordenarPersonas.add("Dinana");

        for (String s: ordenarPersonas) {
            System.out.println(s);
        }*/

      Articulo primero = new Articulo(1, "Primer artículo");
      Articulo segundo = new Articulo(2, "Segundo artículo");
      Articulo tercer  = new Articulo(3, "Tercer artículo");

      TreeSet<Articulo> ordenaArticulo = new TreeSet<Articulo>();

      ordenaArticulo.add(tercer);
      ordenaArticulo.add(primero);
      ordenaArticulo.add(segundo);

        for (Articulo art: ordenaArticulo ) {
            System.out.println(art.getDescripcion());
        }

    }

}

class Articulo implements Comparable<Articulo>{

    private int numero_articulo;
    private String descripcion;

    public Articulo(int num, String desc) {
        numero_articulo = num;
        descripcion = desc;

    }

    public String getDescripcion(){
        return descripcion;
    }

    @Override
    public int compareTo(Articulo o) {
        return numero_articulo - o.numero_articulo;
    }
}
