package Collections;

public class Libro_Video181 {

    private String titulo;
    private String autor;
    private int ISBN;

    public Libro_Video181(String titulo, String autor, int ISBN) {
        this.titulo = titulo;
        this.autor = autor;
        this.ISBN = ISBN;
    }

    @Override
    public String toString() {
        return "Libro{" + "titulo='" + titulo + '\'' + ", autor='" + autor + '\'' + ", ISBN=" + ISBN + '}';
    }
}
