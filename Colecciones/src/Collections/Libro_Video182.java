package Collections;

import java.util.Objects;

public class Libro_Video182 {

    private String titulo;
    private String autor;
    private int ISBN;

    public Libro_Video182(String titulo, String autor, int ISBN) {
        this.titulo = titulo;
        this.autor = autor;
        this.ISBN = ISBN;
    }

    @Override
    public String toString() {
        return "Libro{" + "titulo='" + titulo + '\'' + ", autor='" + autor + '\'' + ", ISBN=" + ISBN + '}';
    }

  /*  public boolean equals(Object obj){

        if (obj instanceof Libro_Video182){
            Libro_Video182 otro = (Libro_Video182)obj;

            if (this.ISBN==otro.ISBN){
              return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Libro_Video182 that = (Libro_Video182) o;
        return ISBN == that.ISBN;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ISBN);
    }
}
