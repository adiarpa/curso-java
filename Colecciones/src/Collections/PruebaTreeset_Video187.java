package Collections;

import java.util.Comparator;
import java.util.TreeSet;

public class PruebaTreeset_Video187 {
    public static void main(String[] args) {
    /*    TreeSet<String> ordenarPersonas = new TreeSet<String>();

        ordenarPersonas.add("Sandra");
        ordenarPersonas.add("Amanda");
        ordenarPersonas.add("Dinana");

        for (String s: ordenarPersonas) {
            System.out.println(s);
        }*/

      Articuloo primero = new Articuloo(1, "Primer artículo");
      Articuloo segundo = new Articuloo(200, "Segundo artículo");
      Articuloo tercer  = new Articuloo(3, "Este es el tercer artículo");

      TreeSet<Articuloo> ordenaArticulo = new TreeSet<Articuloo>();

      ordenaArticulo.add(tercer);
      ordenaArticulo.add(primero);
      ordenaArticulo.add(segundo);

        for (Articuloo art: ordenaArticulo ) {
            System.out.println(art.getDescripcion());
        }

        Articuloo comparadorArticulos = new Articuloo();
        TreeSet<Articuloo> ordenaArticulos2 = new TreeSet<Articuloo>(comparadorArticulos);

        ordenaArticulos2.add(primero);
        ordenaArticulos2.add(segundo);
        ordenaArticulos2.add(tercer);

        for (Articuloo art: ordenaArticulos2) {
            System.out.println(art.getDescripcion());
        }

    }
}

class Articuloo implements Comparable<Articuloo>, Comparator<Articuloo> {

    public Articuloo() {
    }

    private int numero_articulo;
    private String descripcion;

    public Articuloo(int num, String desc) {
        numero_articulo = num;
        descripcion = desc;

    }

    public String getDescripcion(){
        return descripcion;
    }

    @Override
    public int compareTo(Articuloo o) {
        return numero_articulo - o.numero_articulo;
    }

    @Override
    public int compare(Articuloo o1, Articuloo o2) {
        String descripcionA = o1.getDescripcion();
        String descripcionB = o2.getDescripcion();
        return descripcionA.compareTo(descripcionB);
    }
}
