package Collections;

import java.util.Objects;

public class Cliente_Video180 {

    private String nombre;
    private String n_cuenta;
    private double saldo;

    public Cliente_Video180(String nombre, String n_cuenta, double saldo) {
        this.nombre = nombre;
        this.n_cuenta = n_cuenta;
        this.saldo = saldo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getN_cuenta() {
        return n_cuenta;
    }
    public void setN_cuenta(String n_cuenta) {
        this.n_cuenta = n_cuenta;
    }
    public double getSaldo() {
        return saldo;
    }
    public void setSaldo(double saldo) {this.saldo = saldo; }

    /* -- Los siguientes métodos: "equals y hashCode" se utilizan en el video 182
       -- El resto de código es utilizado en el video 181. */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente_Video180 that = (Cliente_Video180) o;
        return n_cuenta.equals(that.n_cuenta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n_cuenta);
    }
}
