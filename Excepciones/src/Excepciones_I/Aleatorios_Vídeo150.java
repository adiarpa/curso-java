package Excepciones_I;

import javax.swing.*;

public class Aleatorios_Vídeo150 {

    public static void main(String[] args) {
        int elementos = Integer.parseInt(JOptionPane.showInputDialog("Introduce la cantidad de elementos de la matriz"));
        int num_aleat[] = new int[elementos];

        for (int i=0; i<num_aleat.length;i++){
            num_aleat[i]=(int)(Math.random()*100); //Se genera los numeros de manera aleatoria.
        }

        for (int elem:num_aleat){
            System.out.println(elem);  //Se imprimen los elementos guardados.
        }

    }

}
